﻿--liquibase formatted sql

--changeset iotdashboard:v1 logicalFilePath:20180502_1414.sql
-- Table: sensor_data
CREATE TABLE ACCESS_DATA
(
  ID serial NOT NULL,
  SOURCE_UID character varying(40),
  CATEGORY character varying(50),
  ACTION character varying(20),
  CARD_ID character varying(50),
  CREATE_TIMESTAMP timestamp without time zone DEFAULT now(),
    CONSTRAINT access_data_pk PRIMARY KEY (id )
);

CREATE TABLE PRESENCE_DATA
(
  ID serial NOT NULL,
  SOURCE_UID character varying(40),
  CATEGORY character varying(50),
  ACTION character varying(20),
  OBJECT_ID character varying(20),
  CREATE_TIMESTAMP timestamp without time zone DEFAULT now(),
    CONSTRAINT presence_data_pk PRIMARY KEY (id )
);