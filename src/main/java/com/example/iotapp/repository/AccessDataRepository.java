package com.example.iotapp.repository;

import com.example.iotapp.entity.AccessData;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccessDataRepository extends PagingAndSortingRepository<AccessData, Long> {

    List<AccessData> findFirst50ByOrderByCreateTimestampDesc();

}
