package com.example.iotapp.repository;

import com.example.iotapp.entity.PresenceData;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PresenceDataRepository extends PagingAndSortingRepository<PresenceData, Long> {

    List<PresenceData> findFirst50ByOrderByCreateTimestampDesc();

}
