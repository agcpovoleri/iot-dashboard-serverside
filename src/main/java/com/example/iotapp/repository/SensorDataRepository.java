package com.example.iotapp.repository;

import com.example.iotapp.entity.SensorCategoryType;
import com.example.iotapp.entity.SensorData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorDataRepository extends PagingAndSortingRepository<SensorData, Long> {

    @Query(value = "SELECT s FROM SensorData s " +
            " WHERE s.categoryType = (?1) AND UPPER(s.content) like upper(?2) ")
    Page<SensorData> findAllBySearchingFilters(SensorCategoryType categoryType, String searchText, Pageable pageableRest);

    List<SensorData> findFirst50ByOrderByCreateTimestampDesc();

    List<SensorData> findAllByOrderByCreateTimestampDesc();

}
