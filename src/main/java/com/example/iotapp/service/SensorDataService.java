package com.example.iotapp.service;

import com.example.iotapp.dto.SearchSensorParameter;
import com.example.iotapp.entity.SensorData;

import java.util.List;

public interface SensorDataService {

    void create(SensorData sensorData);

    SensorData findOne(Long id);
    List<SensorData> findAll();

    List<SensorData> getAllSensorByFilter(SearchSensorParameter searchPrameters);
}
