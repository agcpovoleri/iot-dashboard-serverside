package com.example.iotapp.service;

import com.example.iotapp.entity.SensorCategoryType;
import com.example.iotapp.entity.SensorData;
import org.springframework.data.domain.Page;

public interface SensorService {

    Page<SensorData> findSensorByType(SensorCategoryType type, Integer size, Integer page);

}
