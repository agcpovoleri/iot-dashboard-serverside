package com.example.iotapp.service.integration.mqtt;

public interface MQTTPublisherBase {

    /**
     * Publish message
     *
     * @param topic
     * @param message
     */
    public void publishMessage(String topic, String message);

    /**
     * Disconnect MQTT Client
     */
    public void disconnect();

}
