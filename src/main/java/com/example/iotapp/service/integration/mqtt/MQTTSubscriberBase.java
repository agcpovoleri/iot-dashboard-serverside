package com.example.iotapp.service.integration.mqtt;

public interface MQTTSubscriberBase {

    /**
     * Subscribe message
     *
     * @param topic
     */
    void subscribeMessage(String topic);

    /**
     * Disconnect MQTT Client
     */
    void disconnect();
}
