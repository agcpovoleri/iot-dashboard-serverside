package com.example.iotapp.service;

import com.example.iotapp.entity.AccessData;

import java.util.List;

public interface AccessDataService {

    void create(AccessData accessData);

    AccessData findLast();
    List<AccessData> findAll();

}
