package com.example.iotapp.service;

import com.example.iotapp.entity.PresenceData;

import java.util.List;

public interface PresenceDataService {

    void create(PresenceData presenceData);

    List<PresenceData> findAll();

}
