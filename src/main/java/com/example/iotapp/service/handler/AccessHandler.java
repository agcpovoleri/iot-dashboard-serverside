package com.example.iotapp.service.handler;

import com.example.iotapp.entity.AccessData;
import com.example.iotapp.entity.SensorCategoryType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class AccessHandler extends MqttHandleStrategy<AccessData> {


    Logger logger = LoggerFactory.getLogger(this.getClass());

    List<String> listeningTopics = Lists.newArrayList("access","accessGranted","accessDenied");

    @Override
    public AccessData translateMessage(Message<?> message) throws MessagingException {

        Object payload = message.getPayload();
        logger.info("register access status");
        logger.info("message received on :"+payload.toString());

        String type = null;//retrieve type name
        /*
        {"type":"ACCESS","sourceUID":"NODE02-Door","content":{"action":"OPEN","cardId":" A5 26 0F 02"}}
         */
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(payload.toString());
            JsonNode sourceUID = actualObj.get("sourceUID");
            JsonNode content = actualObj.get("content");
            JsonNode action = content.get("action");
            JsonNode cardId = content.get("cardId");

            AccessData accessData = new AccessData();
            accessData.setCategoryType(SensorCategoryType.ACCESS);
            accessData.setSourceUID(sourceUID.asText());
            accessData.setAction(action.asText());
            accessData.setCardId(cardId.asText());

            return accessData;
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isValid(Message<?> message) throws MessagingException {

        final String mqttPayload = message.getPayload().toString();
        final String mqttTopic = message.getHeaders().toString();

        String type = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(mqttPayload);
            JsonNode typeNode = actualObj.get("type");
            type = typeNode.asText().toUpperCase();
            if (type.equals(SensorCategoryType.ACCESS.toString())) {
                return true;
            }
        } catch (Exception e) {

        }
        return false;

    }
}
