package com.example.iotapp.service.handler;

import com.example.iotapp.entity.PresenceData;
import com.example.iotapp.entity.SensorCategoryType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class PresenceHandler extends MqttHandleStrategy<PresenceData> {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public PresenceData translateMessage(Message<?> message) throws MessagingException {

        Object payload = message.getPayload();
        logger.info("register presence by mqtt :"+message.getHeaders().toString());
        logger.info("message received on :"+payload.toString());

        String type = null;//retrieve type name
        /*
        {"type":"PRESENCE","sourceUID":"ARD-Garage","content":{"action":"IN","objectUID":"AFBC34EF"}}
        {"type":"PRESENCE","sourceUID":"ARD-Garage","content":{"action":"OUT","objectUID":"AFBC34EF"}}
         */
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(payload.toString());
            JsonNode sourceUID = actualObj.get("sourceUID");
            JsonNode content = actualObj.get("content");
            JsonNode action = content.get("action");
            JsonNode objectId = content.get("objectId");

            PresenceData presenceData = new PresenceData();
            presenceData.setCategoryType(SensorCategoryType.PRESENCE);
            presenceData.setSourceUID(sourceUID.asText());
            presenceData.setAction(action.asText());
            presenceData.setObjectId(objectId.asText());

            return presenceData;
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isValid(Message<?> message) throws MessagingException {

        final String mqttPayload = message.getPayload().toString();

        String type = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(mqttPayload);
            JsonNode typeNode = actualObj.get("type");
            type = typeNode.asText().toUpperCase();
            if (type.equals(SensorCategoryType.PRESENCE.toString())) {
                return true;
            }
        } catch (Exception e) {

        }
        return false;

    }
}
