package com.example.iotapp.service.handler;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;

public abstract class MqttHandleStrategy<T> {

    public abstract boolean isValid(Message<?> message) throws MessagingException;

    /**
     * Handle the given message.
     * @param message the message to be handled
     */
    public abstract T translateMessage(Message<?> message) throws MessagingException;


}
