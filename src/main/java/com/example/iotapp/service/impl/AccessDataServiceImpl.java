package com.example.iotapp.service.impl;

import com.example.iotapp.entity.AccessData;
import com.example.iotapp.repository.AccessDataRepository;
import com.example.iotapp.service.AccessDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccessDataServiceImpl implements AccessDataService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AccessDataRepository accessDataRepository;

    @Override
    public void create(AccessData accessData) {

        accessDataRepository.save(accessData);
    }

    @Override
    public AccessData findLast() {
        return null;//accessDataRepository.findFirstByOrderByCreateTimestampDesc();
    }

    public List<AccessData> findAll() {
		return accessDataRepository.findFirst50ByOrderByCreateTimestampDesc();
	}

}
