package com.example.iotapp.service.impl;

import com.example.iotapp.entity.PresenceData;
import com.example.iotapp.repository.PresenceDataRepository;
import com.example.iotapp.service.PresenceDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PresenceDataServiceImpl implements PresenceDataService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PresenceDataRepository presenceDataRepository;

    @Override
    public void create(PresenceData presenceData) {

        presenceDataRepository.save(presenceData);
    }

    public List<PresenceData> findAll() {
		return presenceDataRepository.findFirst50ByOrderByCreateTimestampDesc();
	}

}
