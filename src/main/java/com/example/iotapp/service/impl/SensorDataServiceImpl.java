package com.example.iotapp.service.impl;

import com.example.iotapp.dto.SearchSensorParameter;
import com.example.iotapp.entity.SensorCategoryType;
import com.example.iotapp.entity.SensorData;
import com.example.iotapp.repository.SensorDataRepository;
import com.example.iotapp.service.SensorDataService;
import com.example.iotapp.service.integration.mqtt.MQTTPublisherBase;
import com.example.iotapp.util.PaginationUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SensorDataServiceImpl implements SensorDataService {

    @Autowired
    MQTTPublisherBase mqttPublisher;

    @Autowired
    private SensorDataRepository sensorDataRepository;

    @Override
    public void create(SensorData sensorData) {

        sensorDataRepository.save(sensorData);

//        String topicName = "cefet-agcp/" + sensorData.getCategoryType();
//        mqttPublisher.publishMessage(topicName, sensorData.getContent());
    }

    @Override
	public SensorData findOne(Long id) {
		return sensorDataRepository.findOne(id);
	}

    public List<SensorData> findAll() {
		return sensorDataRepository.findFirst50ByOrderByCreateTimestampDesc();
	}

    @Override
    public List<SensorData> getAllSensorByFilter(SearchSensorParameter searchFilter) {

        Pageable pageRequest = PaginationUtils.createPageRequest(searchFilter.getPage(), searchFilter.getSize());
        Page<SensorData> allFoundPackages = null;

        SensorCategoryType packageCategory = null;

        boolean hasKeyword = !Strings.isNullOrEmpty(searchFilter.getKeyword());
        final String keyword = "%"+searchFilter.getKeyword()+"%";

        boolean hasSensorCategory = searchFilter.getSensorCategory() != null;

        if (hasSensorCategory) {
            packageCategory = SensorCategoryType.findFromCode(searchFilter.getSensorCategory());
        }
        // Search different parts from cache
        if (hasKeyword && hasSensorCategory) {
            allFoundPackages = sensorDataRepository.findAllBySearchingFilters(packageCategory, keyword, pageRequest);
        }

        if (allFoundPackages != null) {
            return allFoundPackages.getContent();
        } else {
            return Lists.newArrayList();
        }
    }
}
