package com.example.iotapp.exception;

import javax.persistence.EntityNotFoundException;

public class SensorCatergoryNotFoundException extends EntityNotFoundException {

    public SensorCatergoryNotFoundException() {
                        super("sensor.category.not.found.exception");
    }
}
