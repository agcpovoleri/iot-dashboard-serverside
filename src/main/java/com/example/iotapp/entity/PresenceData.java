package com.example.iotapp.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PRESENCE_DATA")
public class PresenceData implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

    @Column(name = "CATEGORY")
    @Enumerated(EnumType.STRING)
    private SensorCategoryType categoryType;

    @Column(name = "SOURCE_UID")
    private String sourceUID;

    private String action;

    @Column(name = "OBJECT_ID")
    private String objectId;

    private Date createTimestamp;

    public PresenceData() {
    }

    @PrePersist
    private void setValuesBeforePersist(){
        this.createTimestamp = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SensorCategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(SensorCategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSourceUID() {
        return sourceUID;
    }

    public void setSourceUID(String sourceUID) {
        this.sourceUID = sourceUID;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("categoryType", categoryType)
                .append("sourceUID", sourceUID)
                .append("action", action)
                .append("objectId", objectId)
                .append("createTimestamp", createTimestamp)
                .toString();
    }
}
