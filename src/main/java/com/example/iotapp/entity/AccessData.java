package com.example.iotapp.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ACCESS_DATA")
public class AccessData implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

    @Column(name = "CATEGORY")
    @Enumerated(EnumType.STRING)
    private SensorCategoryType categoryType;

    private String action;

    @Column(name = "CARD_ID")
    private String cardId;

    @Column(name = "SOURCE_UID")
    private String sourceUID;

    private Date createTimestamp;

    public AccessData() {
    }

    @PrePersist
    private void setValuesBeforePersist(){
        this.createTimestamp = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SensorCategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(SensorCategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getSourceUID() {
        return sourceUID;
    }

    public void setSourceUID(String sourceUID) {
        this.sourceUID = sourceUID;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("categoryType", categoryType)
                .append("action", action)
                .append("cardId", cardId)
                .append("sourceUID", sourceUID)
                .append("createTimestamp", createTimestamp)
                .toString();
    }
}
