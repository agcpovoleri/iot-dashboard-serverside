package com.example.iotapp.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum SensorCategoryType {

    GENERIC(1, "Generic sensor data"),
    TEMPERATURE(2, "Temperature"),
    HUMIDITY(3, "Humidity"),
    ENERGY(4, "Energy"),
    VEHICLE(5, "Car location"),
    LIGHTNING(6, "Lights"),
    PRESENCE(7, "Presence"),
    ACCESS(8, "Access Control");

    private Integer codigo;
    private String name;

    SensorCategoryType(Integer codigo, String name) {

        this.codigo = codigo;
        this.name = name;
    }

    private static Map<Integer, SensorCategoryType> map = new HashMap<Integer, SensorCategoryType>();

    static {
        for (SensorCategoryType categoryTypeEnum : SensorCategoryType.values()) {
            map.put(categoryTypeEnum.codigo.intValue(), categoryTypeEnum);
        }
    }

    public static SensorCategoryType findFromCode(Integer codigo) {
        SensorCategoryType category = Optional.ofNullable(map.get(codigo)).orElse(SensorCategoryType.GENERIC);
        return category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
}
