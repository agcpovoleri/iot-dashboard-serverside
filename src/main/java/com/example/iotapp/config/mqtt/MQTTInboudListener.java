package com.example.iotapp.config.mqtt;

import com.example.iotapp.entity.AccessData;
import com.example.iotapp.entity.PresenceData;
import com.example.iotapp.service.AccessDataService;
import com.example.iotapp.service.PresenceDataService;
import com.example.iotapp.service.handler.AccessHandler;
import com.example.iotapp.service.handler.PresenceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;


@Configuration
@EnableIntegration
public class MQTTInboudListener {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AccessDataService accessDataService;

    @Autowired
    PresenceDataService presenceDataService;


    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageProducer inbound() {
        MqttPahoMessageDrivenChannelAdapter adapter =
                new MqttPahoMessageDrivenChannelAdapter("tcp://iot.eclipse.org:1883", "iotDashboardServer",
                        "iot-agcp/access","iot-agcp/presence");

        adapter.setCompletionTimeout(5000);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(2);
        adapter.setOutputChannel(mqttInputChannel());
        adapter.setAutoStartup(true);
        return adapter;
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {

        return new MessageHandler() {

            @Override
            public void handleMessage(Message<?> message) throws MessagingException {

                //Register Handler by validation order
                AccessHandler handler = new AccessHandler();
                if (handler.isValid(message)) {
                    AccessData data = handler.translateMessage(message);
                    if (data != null) accessDataService.create(data);
                    return;
                }

                PresenceHandler presenceHandler = new PresenceHandler();
                if (presenceHandler.isValid(message)) {
                    PresenceData data = presenceHandler .translateMessage(message);
                    if (data != null) presenceDataService.create(data);
                    return;
                }

                //generic handler
                Object payload = message.getPayload();
                logger.info("Generic handle message received on :"+payload.toString());
            }

        };
    }

}
