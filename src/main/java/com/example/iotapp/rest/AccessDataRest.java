package com.example.iotapp.rest;

import com.example.iotapp.dto.AccessDataDetailResponse;
import com.example.iotapp.dto.DtoConverter;
import com.example.iotapp.entity.AccessData;
import com.example.iotapp.service.AccessDataService;
import com.example.iotapp.service.UserLoginService;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("/api/access")
public class AccessDataRest {

	private static final Logger logger = LoggerFactory.getLogger(AccessDataRest.class);

    @Autowired
	private AccessDataService accessDataService;

    @Autowired
    private UserLoginService userLoginService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public List<AccessDataDetailResponse> findAll() {
        return Lists.transform(accessDataService.findAll(), new Function<AccessData, AccessDataDetailResponse>() {
            @Override
            public AccessDataDetailResponse apply(AccessData accessData) {
                return DtoConverter.buildAccessDataDetail(accessData);
            }
        });
	}
}
