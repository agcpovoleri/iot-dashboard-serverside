package com.example.iotapp.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/api/sample")
public class SampleRest {

	private static final Logger logger = LoggerFactory.getLogger(SampleRest.class);

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public void save(String content){
        logger.info("Sensor data: "+content);

    }
}
