package com.example.iotapp.dto;

import javax.validation.constraints.NotNull;

public class SensorDataRequest {

    @NotNull
    private String sourceUID;

    @NotNull
    private Integer type;

    @NotNull
    private String data;

    public String getSourceUID() {
        return sourceUID;
    }

    public void setSourceUID(String sourceUID) {
        this.sourceUID = sourceUID;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}