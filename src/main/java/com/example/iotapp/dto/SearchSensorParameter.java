package com.example.iotapp.dto;

import com.google.common.base.Strings;

public class SearchSensorParameter {

    private Integer sensorCategory;
    private String keyword;
    private Integer page;
    private Integer size;

    public SearchSensorParameter(Integer sensorCategory, String searchText, Integer page, Integer size) {
        this.sensorCategory = sensorCategory;
        this.keyword = searchText;
        this.page = page;
        this.size = size;
    }

    public Integer getSensorCategory() {
        return sensorCategory;
    }

    public String getKeyword() {
        return keyword;
    }

    public Integer getPage() {
        return page;
    }

    public Integer getSize() {
        return size;
    }

    public boolean hasAnyFilledSearchField() {
        return (!Strings.isNullOrEmpty(this.keyword) || this.sensorCategory!=null || this.page != null || this.size != null);
    }

    public boolean isPaginationOnly() {
        return (Strings.isNullOrEmpty(this.keyword) && this.sensorCategory!=null && this.page != null && this.size != null);
    }
}
