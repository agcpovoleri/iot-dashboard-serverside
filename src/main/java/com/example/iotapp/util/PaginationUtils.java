package com.example.iotapp.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public final class PaginationUtils {

    public static Pageable createPageRequest(Integer page, Integer size) {
        return new PageRequest(page, size);
//        return new PageRequest(1, 10, Sort.Direction.ASC, "title", "description");
//        return new PageRequest(1,
//                10,
//                new Sort(Sort.Direction.DESC, "description")
//                        .and(new Sort(Sort.Direction.ASC, "title"));
//        );
    }

}
